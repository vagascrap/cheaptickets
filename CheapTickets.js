//CheapTickets.com Scrapper

var cheerio = require('cheerio'),
 _s = require('underscore.string'),
 fs = require('fs'),
 moment = require('moment');

//Example file to be scrapped 
 var file = './cheaptickets/cheaptickets.html';

//function that returns iteniraries 
var getCheapTicketsItineraries = function(data){

	$ = cheerio.load(data);
	var itineraries = [],
		provider = 'CheapTickets', //Website to be scrapped
		url = '', 
		departureInfoArea = $('.tripSummary.multiCity'), //Area donde se encuentra la informacion sobre la fechas de salida y llegada
		departureInfo = moment($('tr[data-context=multiCityLocation]>td:nth-child(3)'))
			.format('ddd, MMM DD,YYYY'),
		list = $('.airResultsMod.resultSetAir.incremental>div').attr('data-context', 'airResultsCard'),
		departureInfoRawArr = $('td[data-context=multiCityDate]>span', departureInfoArea),
		departureInfoArr=[]; //Arreglo de fechas de salida para el multicity

		departureInfoRawArr.each(function(i,element){

			var date = moment($(element), 'ddd, MMM DD, YYYY')
			.format('ddd, MMM DD,YYYY');

			if(date != 'Invalid date')
				departureInfoArr.push(date)
		});

		list.each(function(i, flight){

			var itinerary ={},
				result=getTrip(flight,departureInfoArr);
				
				itinerary.trips = result[0];
				itinerary.totalCost = result[1];
				itinerary.url = url;
				itinerary.provider = provider;

				itineraries.push(itinerary);

		});


	return itineraries;
}

//Retorna un objeto del viaje
var getTrip = function(flight, departureInfoArr){

	//Para cumplir con el format de objeto Trips[{},{},{}]
	var returnTrip = [];

	//Costo total del viaje
	var totalCost = parseInt($('.basePrice>span>span:first-child', flight).text().replace(',','').replace('$',''));

	//Area donde estan los itinerarios
	var divFlight = $('.airSlice.slice', flight);
 
 	//Extrayendo los segmentos de viaje con sus respectivas fechas	
	divFlight.each(function(i,segment){

		//Objeto que representa el viaje
		var trip = {};

		//Informacion sobre a hora de salida
		trip.departureInfoTime = departureInfoArr[i];

		//Hora de Salida
		trip.departureTime = moment($('.info.departure>span:first-child', segment).text(), 'hh:mma').format('h:mm A');

		//Tiempo Formato Crudo 
		trip.departureTimeRaw = moment(trip.departureInfoTime+' '+trip.departureTime,'ddd, MMM DD,YYYY hh:mm a').toISOString();

		//Ciudad de Salida 
		trip.departureCity = _s.clean($('.info.departure>span:nth-child(2)', segment).text());

		//Aeropuerto de salida
		trip.departureAirport = $('.info.departure>span>abbr', segment).text();

		//Tiempo de llegada
		trip.arrivalTime = moment($('.info.infoArrival>div>span:first-child',segment).text(),'hh:mma').format('h:mm A');

		//Aeropuerto de salida
		trip.arrivalAirport =  $('.info.infoArrival>div>span>abbr',segment).text();

		//Ciudad de llegada
		trip.arrivalCity = _s.clean($('.info.infoArrival>div>span:nth-child(2)',segment).text());

		//Numero de paradas que habra en el viaje
		trip.stops = parseInt($('.info.stops>span:first-child',segment).text().replace(' ','').replace('stops',''));

		//Tiempo que durara el viaje
		trip.duration = $('.info.stops>span:nth-child(2)',segment).text().replace('r ','').replace('in', '');

		//Eliminar basura para dejar numeros solos
		var durArr = trip.duration
		.replace('h',' ')
		.replace('m',' ')
		.split(' ');

		//Objeto parseado para separar minutos, horas y días.
		trip.durationObj={};
		trip.durationObj.days = parseInt(durArr[0]/24); //Dias
		trip.durationObj.hours = durArr[0]%24;			//Horas
		trip.durationObj.mins = parseInt(durArr[1]);	//Minutos

		//Viaje operado por
		trip.operatedBy = _s.clean($('.operatedBy',segment).text()); 

		//Nombre de la Aerolinea
		trip.airline= _s.clean($('.flightInfo>ul>li>strong',segment).text()); 

		//Codigo de la aerolinea.  El codigo es 00 si son aerolineas multiples
		trip.airLineCode = $('.logo>img',segment).attr('data-context').split('.')[3];

/*
		trip.mileage: '', //Número entero [opcional]. Incluir el número de millas si se cuenta con el dato
        trip.originTerminal: "", //[opcional] La terminal del aeropuerto donde saldrá
        trip.destinationTerminal: "", //[opcional] La terminal del aeropuerto donde llega
	    trip.meal: "" //[opcional] El tipo de comida que se sirve
*/
		//Insertando segmento de viaje trips[{},{},{}]
		returnTrip.push(trip);
	});

	return [returnTrip,totalCost];
}

//Funcion para abrir el archivo a parsear.
fs.readFile(file, {encoding: 'utf-8'}, function (err, data) {
	if (err) throw err;

	var itineraries=getCheapTicketsItineraries(data);
	console.log(JSON.stringify(itineraries));
});